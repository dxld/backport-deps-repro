#!/bin/sh

main () {
	cleanup "$@"
	set -e

	# Create archive for stable
	make_stub_pkg libgoo 1.0
	update_archive stable

	# Create initial archive for backports
	make_stub_pkg libgoo 5.0~bpo+1
	make_stub_pkg foo    1.2~bpo+1
	update_archive backports \
		       "NotAutomatic: yes" \
		       "ButAutomaticUpgrades: yes"

	setup_apt

	fake_dpkg_install foo 1.2~bpo+1

	apt-get update
	apt-cache policy libgoo
	apt-cache policy foo

	# So far so good, now foo is updated in backports and depends on
	# new a new libgoo version
	make_stub_pkg libgoo 6.0~bpo+1
	make_stub_pkg foo    1.3~bpo+1 'libgoo (= 6.0~bpo+1)'
	update_archive backports \
		       "NotAutomatic: yes" \
		       "ButAutomaticUpgrades: yes"

	apt-get update
	apt-cache policy libgoo
	apt-cache policy foo

	apt-get install -s foo=1.3~bpo+1
}

cleanup () {
	# Setup
	rm pkgs *.deb *.bin *.list *.conf
	rm -rf foo*/ libgoo*/ stable/ backports/ lists/ archives/ apt/ dpkg/
	[ "$1" != clean ] || exit
}

make_stub_pkg () {
	name=$1; ver=$2; shift 2
	mkdir -p "${name}_${ver}"/DEBIAN
	cat > "${name}_${ver}"/DEBIAN/control <<-EOF
		Package: $name
		Version: $ver
		Description: $name
		Maintainer: nobody
		Architecture: all
		Depends: $*

	EOF
	dpkg-deb --build "${name}_${ver}"
	echo "${name}_${ver}".deb >> pkgs
}

# Copy Create Packages and Release files for a flat archive
update_archive () {
	suite=$1; shift
	mkdir -p "$suite"
	cat pkgs
        xargs cp -t "$suite"/ < pkgs
	echo -n > pkgs
        apt-ftparchive packages "$suite"/ > "$suite"/Packages
        apt-ftparchive release	"$suite"/ > "$suite"/Release
	printf "%s\n" \
	       "Suite: $suite" \
	       "$@" \
	       >> "$suite"/Release
}

# Make dpkg think the given package installed
fake_dpkg_install () {
        pkg=$1; ver=$2; shift 2
	mkdir -p dpkg/updates
	echo "Status: install ok installed" >> dpkg/status
	cat "${pkg}_${ver}"/DEBIAN/control >> dpkg/status
}

# Create apt configs to use just our two fake archives and nothing else
setup_apt () {
	cat > sources.list <<-EOF
		deb [trusted=true] file:$PWD/stable ./
		deb [trusted=true] file:$PWD/backports ./
	EOF

	mkdir -p apt/ dpkg/
	touch dpkg/status
	cat > apt.conf <<-EOF
		Dir "$PWD/apt";
		Debug::NoLocking true;

		Dir::State $PWD/apt;
		Dir::State::Lists lists;
		Dir::State::status $PWD/dpkg/status;

		Dir::Etc $PWD;
		Dir::Etc::main /dev/null;
		Dir::Etc::SourceList sources.list;

		Dir::Etc::parts "";
		Dir::Etc::netrcparts "";
		Dir::Etc::sourceparts "";
		Dir::Etc::preferencesparts "";

		Dir::Cache $PWD;
	EOF

	export APT_CONFIG=$PWD/apt.conf
}

main "$@"
